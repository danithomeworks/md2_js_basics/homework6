// Завдання
// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.

// Створити метод getAge() який повертатиме скільки користувачеві років.

// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі,
// з'єднану з прізвищем (у нижньому регістрі) та роком народження.
// (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.

// Вивести в консоль результат роботи функції createNewUser(),
// а також функцій getAge() та getPassword() створеного об'єкта.

"use strict";

function createNewUser() {
  let newUser = {
    _firstName: "",
    _lastName: "",
    _birthday: "",

    get firstName() {
      return this._firstName;
    },

    set firstName(newFirstName) {
      this._firstName = newFirstName;
    },

    get lastName() {
      return this._lastName;
    },

    set lastName(newLastName) {
      this._lastName = newLastName;
    },

    get birthday() {
      return this._birthday;
    },

    set birthday(newBirthday) {
      this._birthday = newBirthday;
    },

    getLogin() {
      return (this._firstName[0] + this._lastName).toLowerCase();
    },

    getAge() {
      return (
        new Date(
          Date.parse(new Date()) -
            Date.parse(
              // // Converting birthday date to mm.dd.yyyy format
              `${this._birthday.split(".")[1]}.${
                this._birthday.split(".")[0]
              }.${this._birthday.split(".")[2]}`
            )
        ).getFullYear() - 1970
      );
    },

    getPassword() {
      return (
        this._firstName[0].toUpperCase() +
        this._lastName.toLowerCase() +
        this._birthday.split(".")[2]
      );
    },
  };
  return newUser;
}

let userObject = createNewUser();

userObject.firstName = "Test";
userObject.lastName = "User";
userObject.birthday = "17.06.1980";

console.log(userObject);
console.log("User age:", userObject.getAge());
console.log("Password:", userObject.getPassword());
